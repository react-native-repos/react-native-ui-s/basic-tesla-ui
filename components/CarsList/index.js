import React from "react";
import { View, FlatList, Dimensions } from "react-native";
import CarItem from "../CarItem";
import styles from "./styles";
import carData from "./cars";

function CarsList() {
  return (
    <View style={styles.container}>
      {/* in the tesla website it tries to display the cloest component depending on where we are in the scroll position */}
      {/* snap to alignment can be used to achieve this effect in rn */}
      <FlatList
        data={carData}
        snapToAlignment="start"
        decelerationRate="fast"
        showsVerticalScrollIndicator={false}
        snapToInterval={Dimensions.get("window").height}
        renderItem={({ item }) => <CarItem car={item} />}
      />
    </View>
  );
}

export default CarsList;
