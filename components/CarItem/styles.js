import { StyleSheet, Dimensions } from "react-native";

const styles = StyleSheet.create({
  carContainer: {
    width: "100%",
    // when we try and use the height of 100% of a flatlist, the flatlist isnt working correctly as the height is now very long
    // need to say the height is the height of the screen
    // component now renders on the whole screen
    height: Dimensions.get("window").height,
  },
  titles: {
    marginTop: "30%",
    width: "100%",
    alignItems: "center",
  },
  title: {
    fontSize: 40,
    fontWeight: "500",
  },
  subtitleCTA: {
    textDecorationLine: "underline",
  },
  subtitle: {
    fontSize: 18,
    color: "#5c5e62",
  },
  image: {
    width: "100%",
    height: "100%",
    // cover - try to cover the whole space you give, even though you will not see all the parts of the img if its too wide/tall
    // contain - try to show the whole img, however you will see some white spaces around
    resizeMode: "cover",
    position: "absolute", // so it doesnt push components/text underneath the img
  },
  buttonsContainer: {
    position: "absolute",
    bottom: 50,
    width: "100%",
  },
});

export default styles;
